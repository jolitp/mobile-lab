﻿using UnityEngine;
using System.Collections;

public class MovementFromAcelerometer : MonoBehaviour
{
    public float DefaultForce = 300;

    private Rigidbody _rigidbody;

	// Use this for initialization
	void Start ()
	{
	    _rigidbody = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update ()
	{
	    var x = Input.acceleration.x*DefaultForce*Time.deltaTime;
        var z = Input.acceleration.z*DefaultForce*Time.deltaTime;

	    _rigidbody.velocity = new Vector3(x, 0 , z);
	}
}
